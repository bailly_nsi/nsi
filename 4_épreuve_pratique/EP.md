# EP en 2021

[lien](https://pixees.fr/informatiquelycee/term/ep/index.html) vers sujets



### sujet 1

- recherche
- recherche minimum distance

### sujet 2

- moyenne
- tri liste de 0 et 1

### sujet 3

- multiplication sans *
- dichotomie

### sujet 4

- moyenne
- dichotomie

### sujet 5

- binaire vers décimal
- tri par insertion

### sujet 6

- rendu de monnaie
- file, POO

### sujet 7

- fibonacci
- meilleur note

### sujet 8

- recherche 
- rendu de monnaie

### sujet 9

- moyenne coefficientée
- triangle Pascal dynamique

### sujet 10

- maxi
- pile, recherche des positifs

### sujet 11

- décimal vers binaire
- tri bulle

### sujet 12

- maxi 
- recherche motif dans phrase

### sujet 13

- tri selection
- jeu du "plus ou moins"

### sujet 14

- recherche
- moyenne avec dico

### sujet 15

- recherche min et max
- jeu de carte en POO

### sujet 16

- moyenne
- dec to bin


### sujet 17

- indice min
- separer les 0 et 1 dans une liste

### sujet 18

- recherche
- insertion dans une liste triée croissante

### sujet 19

- recherche
- code César

### sujet 20

- mini
- est_palindome

### sujet 21

- nombre de répetitions dans une liste
- du décimal vers le binaire

### sujet 22

- recherche
- rendu de monnaie

### sujet 23

- nombre d'occurence avec dictionnaire
- fusion de deux listes triées croissantes

### sujet 24

- recherche
- adresse IP

### sujet 25

- recherche
- pixel, recursif

### sujet 26

- occurence max
- image, négatif

### sujet 27

- moyenne
- image, zoom, coeur

### sujet 28

- arbre
- tri itératif, selection

### sujet 29

- termes d'une suite
- mot parfait

### sujet 30

- recherche
- chercher : diviser pour régner
