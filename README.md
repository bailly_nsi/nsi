# Enseignement de spécialité NSI (M. BAILLY)

```python
for i in range(10):
    print(i)
``` 


#### Première NSI

- programmation : type, expression, variables, structures de controle, fonctions, mise au point
- algorithmique : tri, recherche, gloutons, KNN, complexité, correction
- données : types simples (int, float, bool, str) , types construits (list, dict)
- projet : programmation interface graphique  avec python

#### Terminale NSI

- programmation : récursivité
- structure de données : arbres, arbres binaires de recherche
- algorithmique : diviser pour régner, complexité, programmation dynamique, recherche textuelle
- projet : 


